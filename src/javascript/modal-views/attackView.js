import View from "../shared-elements/view";

class AttackView extends View {
  constructor(fighter, updateFighter) {
    super();
    this.fighter = fighter;
    this.updateFighter = updateFighter;
    this.createAttack();
  }

  createAttack() {
    this.element = this.createElement({
      tagName: "li",
      className: "fighter-attack"
    });
    const text = this.createElement({ tagName: "span" });
    text.innerText = `Attack: ${this.fighter.attack}`;
    const btn = this.createEditBtn();
    this.element.append(text, btn);
  }

  createEditBtn() {
    const btn = this.createElement({ tagName: "button", className: "btn" });
    btn.innerText = "edit";
    btn.addEventListener("click", () => this.onEditClick(), false);
    return btn;
  }

  createSaveBtn() {
    const btn = this.createElement({
      tagName: "button",
      className: "btn"
    });
    btn.innerText = "save";
    btn.addEventListener("click", () => this.onSaveClick(), false);
    return btn;
  }

  onEditClick() {
    const input = this.createElement({
      tagName: "input",
      className: "attack-modal-input"
    });
    const btn = this.createSaveBtn();
    this.element.innerHTML = "";
    this.element.append(input, btn);
  }

  async onSaveClick() {
    try {
      const val = document.querySelector(".attack-modal-input").value;
      if (+val) {
        const { status } = await this.updateFighter({
          ...this.fighter,
          attack: +val
        });
        if (status === 200) {
          const text = this.createElement({ tagName: "span" });
          text.innerText = `Attack: ${val}`;
          const btn = this.createEditBtn();
          this.element.innerHTML = "";
          this.element.append(text, btn);
        }
      }
    } catch (error) {
      throw error;
    }
  }
}

export default AttackView;
