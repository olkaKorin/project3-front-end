import View from "../shared-elements/view";
import ModalListView from "./modalListView";
import CloseBtn from "../shared-elements/closeBtn";

class ModalView extends View {
  constructor(fighter, updateFighter) {
    super();
    this.createModal(fighter, updateFighter);
  }

  createModal(fighter, updateFighter) {
    const listElement = new ModalListView(fighter, updateFighter).element;
    const closeBtnElement = new CloseBtn().element;
    closeBtnElement.addEventListener(
      "click",
      event => this.element.remove(),
      false
    );
    const titleElement = this.createTitle();
    this.element = this.createElement({ tagName: "div", className: "modal" });
    this.element.append(closeBtnElement, titleElement, listElement);
  }

  createTitle() {
    const title = this.createElement({
      tagName: "h2",
      className: "modal-title"
    });
    title.innerText = "Fighter's details";
    return title;
  }
}

export default ModalView;
