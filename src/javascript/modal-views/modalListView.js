import View from "../shared-elements/view";
import HealthView from "./healthView";
import AttackView from "./attackView";

class ModalListView extends View {
  constructor(fighter, updateFighter) {
    super();
    this.createModalList(fighter, updateFighter);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "li",
      className: "fighter-name"
    });
    nameElement.innerText = `Name: ${name}`;
    return nameElement;
  }

  createDefense(defense) {
    const defenseElement = this.createElement({
      tagName: "li",
      className: "fighter-defense"
    });
    defenseElement.innerText = `Defense: ${defense}`;
    return defenseElement;
  }

  createModalList(fighter, updateFighter) {
    const { name, health, attack, defense } = fighter;
    const nameElement = this.createName(name);
    const healthElement = new HealthView(fighter, updateFighter).element;
    const attackElement = new AttackView(fighter, updateFighter).element;
    const defenseElement = this.createDefense(defense);
    this.element = this.createElement({
      tagName: "ul",
      className: "modal__list"
    });
    this.element.append(
      nameElement,
      healthElement,
      attackElement,
      defenseElement
    );
  }
}

export default ModalListView;
