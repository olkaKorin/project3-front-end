import FightersView from "./fightersView";
import { fetchFighters } from "./api/fighters";
import StartPageView from "./startPageView";

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById("root");
  static loadingElement = document.getElementById("loading-overlay");

  async startApp() {
    try {
      App.loadingElement.style.visibility = "visible";

      const { data } = await fetchFighters();
      const fightersView = new FightersView(data);
      const fightersElement = fightersView.element;
      const startView = new StartPageView();
      const startElement = startView.element;
      App.rootElement.appendChild(fightersElement);
      App.rootElement.appendChild(startElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = "Failed to load data";
    } finally {
      App.loadingElement.style.visibility = "hidden";
    }
  }
}

export default App;
