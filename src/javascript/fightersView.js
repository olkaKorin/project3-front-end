import View from "./shared-elements/view";
import FighterView from "./fighterView";
import ModalView from "./modal-views/modalView";
import Fighter from "./fighter";
import Header from "./header/header";
import { fetchFighter, updateFighter, createNewFighter } from "./api/fighters";

class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.editFighter = this.editFighter.bind(this);
    this.updateFighter = this.updateFighter.bind(this);
    this.createNewFighter = this.createNewFighter.bind(this);
    this.createFighters(fighters);
  }
  static rootElement = document.getElementById("root");

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const headerElement = new Header(this.createNewFighter).element;
    const fighterElements = fighters.map(fighter => {
      console.log(fighter);
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(headerElement, ...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    document.querySelectorAll(".modal").forEach(item => item.remove());

    try {
      const fighterDetails = await this.getFighter(fighter._id);
      const modalView = new ModalView(fighterDetails, this.updateFighter, () =>
        console.log("hello")
      );
      FightersView.rootElement.append(modalView.element);
    } catch (error) {
      throw error;
    }
  }

  async getFighter(fighterId) {
    const fighter = await this.fightersDetailsMap.get(fighterId);
    if (fighter) {
      return fighter;
    } else {
      const { data } = await fetchFighter(fighterId);
      return this.editFighter(data);
    }
  }

  async editFighter(details) {
    const fighter = new Fighter(details);
    await this.fightersDetailsMap.set(details._id, fighter);
    return fighter;
  }

  async updateFighter(data) {
    const { status } = await updateFighter(data);
    if (status === 200) {
      const fighter = await this.getFighter(data._id);
      fighter.update(data);
      this.fightersDetailsMap.set(data._id, fighter);
    }
    return { status };
  }

  async createNewFighter(req) {
    const { status, data } = await createNewFighter(req);
    if (status === 200) {
      const fighterElement = new FighterView(data, this.handleClick).element;
      this.element.append(fighterElement);
    }
  }
}

export default FightersView;
