import View from "../shared-elements/view";
import CloseBtn from "../shared-elements/closeBtn";

class AddModalView extends View {
  constructor(createNewFighter) {
    super();
    this.createNewFighter = createNewFighter;
    this.createAddModalView();
  }

  createAddModalView() {
    this.element = this.createElement({
      tagName: "div",
      className: "add-modal"
    });
    const closeBtnElement = new CloseBtn().element;
    closeBtnElement.addEventListener(
      "click",
      event => this.element.remove(),
      false
    );
    const titleElement = this.createTitle();
    const nameInput = this.createNameInput();
    const healthInput = this.createHealthInput();
    const attackInput = this.createAttackInput();
    const defenseInput = this.createDefenseInput();
    const sourceInput = this.createSourceInput();
    const submitBtn = this.createSubmitBtn();
    this.element.append(
      closeBtnElement,
      titleElement,
      nameInput,
      healthInput,
      attackInput,
      defenseInput,
      sourceInput,
      submitBtn
    );
  }

  createTitle() {
    const title = this.createElement({
      tagName: "h2",
      className: "modal-title"
    });
    title.innerText = "create a new fighter";
    return title;
  }

  createNameInput() {
    const nameInput = this.createElement({
      tagName: "input",
      className: "name-input"
    });
    nameInput.placeholder = "Your name, Sir!";
    return nameInput;
  }

  createHealthInput() {
    const healthInput = this.createElement({
      tagName: "input",
      className: "health-input"
    });
    healthInput.placeholder = "Do you need some heath?";
    healthInput.type = "number";
    return healthInput;
  }

  createAttackInput() {
    const attackInput = this.createElement({
      tagName: "input",
      className: "attack-input"
    });
    attackInput.placeholder = "How much attack do you need?";
    attackInput.type = "number";
    return attackInput;
  }

  createDefenseInput() {
    const defenseInput = this.createElement({
      tagName: "input",
      className: "defense-input"
    });
    defenseInput.placeholder = "Maybe some defense?";
    defenseInput.type = "number";
    return defenseInput;
  }

  createSourceInput() {
    const sourceInput = this.createElement({
      tagName: "input",
      className: "source-input"
    });
    sourceInput.placeholder = "Please send me a link to your newest photo:)";
    return sourceInput;
  }

  createSubmitBtn() {
    const submitBtn = this.createElement({
      tagName: "button",
      className: "submit-btn"
    });
    submitBtn.innerText = "submit";
    submitBtn.addEventListener("click", () => {
      const name = document.querySelector(".name-input").value;
      const health = document.querySelector(".health-input").value;
      const attack = document.querySelector(".attack-input").value;
      const defense = document.querySelector(".defense-input").value;
      const source = document.querySelector(".source-input").value;
      this.createNewFighter({
        name,
        health: Number(health),
        attack: Number(attack),
        defense: Number(defense),
        source
      });
    });
    return submitBtn;
  }
}

export default AddModalView;
