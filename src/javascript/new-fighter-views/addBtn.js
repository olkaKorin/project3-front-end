import View from "../shared-elements/view";
import AddModalView from "./addModalView";
import { createNewFigther } from "../api/fighters";

class AddBtn extends View {
  constructor(createNewFighter) {
    super();
    this.createNewFighter = createNewFighter;
    this.createAddBtn();
  }
  static rootElement = document.getElementById("root");

  createAddBtn() {
    this.element = this.createElement({
      tagName: "button",
      className: "add-btn"
    });
    this.element.innerText = "add";
    this.element.addEventListener("click", () => this.onAddClick());
    return this.element;
  }

  onAddClick() {
    const modalElement = new AddModalView(this.createNewFighter).element;
    AddBtn.rootElement.appendChild(modalElement);
  }
}

export default AddBtn;
