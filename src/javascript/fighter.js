import { getRandomNum } from "./helpers/utils";

class Fighter {
  constructor(attrs) {
    this.name = attrs.name;
    this._id = attrs._id;
    this.health = attrs.health;
    this.defense = attrs.defense;
    this.attack = attrs.attack;

    this.getHitPower = this.getHitPower.bind(this);
    this.getBlockPower = this.getBlockPower.bind(this);
    this.update = this.update.bind(this);
  }

  getHitPower = () => {
    const criticalHitChance = getRandomNum(1, 2);
    return this.attack * criticalHitChance;
  };

  getBlockPower = () => {
    const dodgeChance = getRandomNum(1, 2);
    return this.defense * dodgeChance;
  };

  update = ({ health, attack }) => {
    this.health = health;
    this.attack = attack;
  };
}

export default Fighter;
