import View from "./shared-elements/view";

class StartPageView extends View {
  constructor() {
    super();
    this.createStartPage();
  }

  createStartPage() {
    this.element = this.createElement({
      tagName: "div",
      className: "start-page"
    });
    const leftSideElement = this.createElement({
      tagName: "div",
      className: "right-start-page"
    });
    const imgElement = this.createImage();
    leftSideElement.append(imgElement);
    const rightSideElement = this.createElement({
      tagName: "div",
      className: "left-start-page"
    });
    const titleElement = this.createTitle();
    const btnElement = this.createBtn();
    rightSideElement.append(titleElement, btnElement);
    btnElement.addEventListener("click", event => this.element.remove(), false);
    this.element.append(leftSideElement, rightSideElement);
  }

  createTitle() {
    const title = this.createElement({
      tagName: "h2",
      className: "start-title"
    });
    title.innerText = "Street fighter";
    return title;
  }

  createBtn() {
    const btn = this.createElement({
      tagName: "button",
      className: "start-btn"
    });
    btn.innerText = "Press to start";
    return btn;
  }

  createImage() {
    const img = this.createElement({
      tagName: "img",
      className: "start-image"
    });
    img.src = "https://i.ibb.co/ggh6Jb2/59e5df4a5bafe3ca532f1e5c-copy.jpg";
    return img;
  }
}

export default StartPageView;
