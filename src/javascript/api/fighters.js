import axios from "axios";
const apiUrl = "https://project3-back-end.olkakorin.now.sh";

export const fetchFighters = () => {
  try {
    return axios.get(`${apiUrl}/fighters`);
  } catch (error) {
    console.log(error);
  }
};

export const fetchFighter = id => {
  try {
    return axios.get(`${apiUrl}/fighter/${id}`);
  } catch (error) {
    console.log(error);
  }
};

export const updateFighter = data => {
  try {
    return axios.put(`${apiUrl}/fighter`, data);
  } catch (error) {
    console.log(error);
  }
};

export const createNewFighter = data => {
  try {
    return axios.post(`${apiUrl}/fighter`, data);
  } catch (error) {
    console.log(error);
  }
};

export const deleteFighter = id => {
  try {
    return axios.delete(`${apiUrl}/fighter/${id}`);
  } catch (error) {
    console.log(error);
  }
};
