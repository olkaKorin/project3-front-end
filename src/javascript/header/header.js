import View from "../shared-elements/view";
import AddBtn from "../new-fighter-views/addBtn";
import FightBtn from "./fightBtn";

class Header extends View {
  constructor(createNewFighter) {
    super();
    this.createHeader(createNewFighter);
  }

  createHeader(createNewFighter) {
    const fightBtnElement = new FightBtn().element;
    const addBtnElement = new AddBtn(createNewFighter).element;

    this.element = this.createElement({
      tagName: "div",
      className: "header"
    });
    this.element.append(addBtnElement, fightBtnElement);
  }
}

export default Header;
