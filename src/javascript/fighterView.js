import View from "./shared-elements/view";
import { deleteFighter } from "./api/fighters";

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();
    this._id = fighter._id;
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const closingElement = this.createClosingElement();
    const choiceElement = this.createChoiceElement();
    this.element = this.createElement({ tagName: "div", className: "fighter" });
    this.element.append(
      imageElement,
      nameElement,
      closingElement,
      choiceElement
    );
    this.element.addEventListener(
      "click",
      event => handleClick(event, fighter),
      false
    );
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name"
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes
    });

    return imgElement;
  }

  createClosingElement() {
    const closingElement = this.createElement({
      tagName: "div",
      className: "closing-elem"
    });
    closingElement.innerText = "x";
    closingElement.addEventListener(
      "click",
      async event => {
        event.stopPropagation();
        const { status } = await deleteFighter(this._id);
        if (status === 200) this.element.remove();
      },
      false
    );
    return closingElement;
  }

  createChoiceElement() {
    const choiceElement = this.createElement({
      tagName: "input",
      className: "fighter-choice"
    });
    choiceElement.type = "checkbox";
    choiceElement.addEventListener(
      "click",
      async event => {
        event.stopPropagation();
        () => this.chooseTwoFighters();
      },
      false
    );
    return choiceElement;
  }

  chooseTwoFighters() {
    console.log("Hey");
    let arr = new Array(2);

    const checkbox = document.querySelectorAll(".fighter-choice");
    if (checkbox.checked == true) {
      arr.push(checkbox);
    }
  }
}

export default FighterView;
