import View from "./view";

class CloseBtn extends View {
  constructor() {
    super();
    this.createCloseButton();
  }

  createCloseButton() {
    this.element = this.createElement({
      tagName: "button",
      className: "modal__close-btn"
    });
    this.element.innerText = "x";
    return this.element;
  }
}

export default CloseBtn;
